import Discord from 'discord.js';

const ready = {
  name: 'ready',
  once: true,
  execute(client) {
    console.log(`Ready! Logged in as ${client.user.tag}. Discord.js version: ` + Discord.version);
  },
};

export default ready;
