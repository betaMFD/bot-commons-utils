
/**
 * This handles hubot-like script files that export a regex and an execute
 */
export const handleScripts = {
  name: 'handleScripts',
  async execute(message) {
    // ignore my own messages
    if (message.author.id === message.client.user.id) {
      return;
    }
    // loop through scripts and look for regex
    const scripts = message.client.scripts;
    for (const [key, s] of scripts) {
      // Remove the bot @ no matter what.
      // If the bot tag isn't required, it will still run if tagged anyway
      const bot_id = message.client.application.id;
      const pattern = `<@\\&?${bot_id}>\\s*`;
      const regex = new RegExp(pattern);
      const content = message.content.replace(regex, '');

      if (s.tag_bot) {
        // If the bot is supposed to start the message off
        // Either DM message, or use mentions
        if (message.guildId === null) {
          // This is a DM. Do the thing.
          if (checkContent(content, s)) {
            await executeScript(message, s);
          }
        } else if (message.mentions.users.size !== 0) {
          if (message.mentions.users.first().id === bot_id) {
            // Only do more regex if the bot is actually mentioned in the message
            // Remove the bot tag from the message so I can match some regex on the rest of the message
            if (checkContent(content, s)) {
              await executeScript(message, s);
            }
          }
        }
      } else {
        // If the bot doesn't need to be tagged, just match the regex
        if (checkContent(content, s)) {
          await executeScript(message, s);
        }
      }
    }
  }
}

function checkContent(content, script) {
  const regex = new RegExp(script.regex);
  return content.match(regex);
}

async function executeScript(message, script) {
  try {
    // Call the script with the message and arguments
    await script.execute(message);
  } catch (error) {
    console.error(error);
    message.reply('There was an error trying to execute the command!');
  }
}
