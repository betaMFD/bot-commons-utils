import path from 'path';
import { readdir } from 'fs/promises';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const messageCreate = {
  name: 'messageCreate',
  async execute(message) {
    // run multiple things onMessageCreate
    const eventsPath = path.resolve(process.cwd(), 'src/events/messageCreateListeners');
    const eventFiles = await readdir(eventsPath);
    const jseventFiles = eventFiles.filter(file => file.endsWith('.mjs'));

    // Include the local handleScripts.mjs file
    const localFilePath = path.resolve(__dirname, './messageCreateListeners/handleScripts.mjs');
    const localEventFiles = [localFilePath];

    // Merge the event files from the bot and the local file
    const allEventFiles = [...jseventFiles.map(file => path.join(eventsPath, file)), ...localEventFiles];

    for (const file of allEventFiles) {
      const filePath = `file://${file}`;
      const messageCreateEvent = await import(filePath);
      const scriptNames = Object.keys(messageCreateEvent);
      for (const scriptName of scriptNames) {
        const script = messageCreateEvent[scriptName];
        script.execute(message);
      }
    }
  },
};

export default messageCreate;
