import { config } from 'dotenv';
config(); // Load environment variables from .env file

import { deployCommands } from '../utils/deployCommands.mjs';

const botRoot = process.argv[2];
const guildId = process.env.GUILD_ID;

deployCommands(botRoot, guildId)
  .then(() => {
    console.log('Guild commands deployment completed successfully.');
  })
  .catch((error) => {
    console.error('Error during guild commands deployment:', error);
  });
