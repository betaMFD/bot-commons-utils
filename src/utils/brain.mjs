import { Low } from 'lowdb';
import { JSONFile } from 'lowdb/node'
import path from 'path';

import { config } from 'dotenv';
config(); // Load environment variables from .env file

if (!process.env.BRAIN_DIR) {
  console.error(`Error: Required environment variable BRAIN_DIR is not set.`);
  process.exit(1);
}

const adapter = new JSONFile(path.resolve(process.cwd(), process.env.BRAIN_DIR, 'brain.json'));
const brain = new Low(adapter);

// Initialize the database
async function initBrain() {
  await brain.read(); // Load the database from file

  // deal with the brainless condition
  if (brain.data == null) {
    brain.data = {};
  }

  // Write any changes to the database back to the file
  await brain.write();

  return brain.data;
}

/**
 * This was rewritten by ChatGPT and I have no idea how it actually works
 * It will navigate through the brain.data object accordingly,
 * creating new objects at each level if they do not exist.
 *
 * @param keys each nested key separated by a comma
 * @returns {*}
 */
function ensureBrainKeyExists(...keys) {
  // Recursive helper function
  function navigateKeys(data, ...remainingKeys) {
    if (remainingKeys.length === 0) {
      return data;
    }

    const [firstKey, ...otherKeys] = remainingKeys;

    if (!data[firstKey]) {
      data[firstKey] = {};
    }

    return navigateKeys(data[firstKey], ...otherKeys);
  }

  return navigateKeys(brain.data, ...keys);
}

/**
 * Same as ensureBrainKeyExists, except returns a promise
 * because sometimes you need that, but sometimes you don't
 * so this gives you a choice
 */
function ensureBrainKeyExistsPromise(...keys) {
  return new Promise((resolve, reject) => {
    try {
      const result = ensureBrainKeyExists(...keys);
      resolve(result);
    } catch (error) {
      reject(error);
    }
  });
}

/**
 * Determine if the brain key exists.
 * Returns the brain object if that key exists, or false
 * @param keys
 * @returns {*|boolean}
 */
function brainKeyOrFalse(...keys) {
  // Helper function to navigate through the keys
  function navigateKeys(data, ...remainingKeys) {
    if (remainingKeys.length === 0) {
      return data;
    }

    const [firstKey, ...otherKeys] = remainingKeys;

    if (data[firstKey] === undefined) {
      return false;
    }

    return navigateKeys(data[firstKey], ...otherKeys);
  }

  return navigateKeys(brain.data, ...keys);
}


// Export the 'initBrain' function and the 'brain' object for use in other files
export { initBrain, brain, ensureBrainKeyExists, ensureBrainKeyExistsPromise, brainKeyOrFalse };
