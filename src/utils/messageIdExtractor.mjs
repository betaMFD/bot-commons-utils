/**
 * Given a message link or a message ID, returns the message ID
 * @param {string} messageLinkOrId
 * @returns {string}
 */
export function extractMessageId(messageLinkOrId) {
  const messageLinkPattern = /https:\/\/discord\.com\/channels\/\d+\/\d+\/(\d+)/;
  const match = messageLinkOrId.match(messageLinkPattern);

  if (match) {
    return match[1]; // Return the extracted message ID from the link
  }

  return messageLinkOrId; // If no match, assume it's a message ID already
}
