import {TextChannel} from 'discord.js';

/**
 * Be cautious with this function. It will clear out all unpinned messages in a channel!
 *
 * DiscordJS should handle rate limiting concerns, but there are 2s delays as well to ensure less discord issues
 *
 * @var {TextChannel}
 * */
export async function deleteAllUnpinnedMessages(channel) {
  let messagesToDelete;
  do {
    // Fetch the messages
    const fetchedMessages = await channel.messages.fetch({ limit: 100 });
    // Filter out the pinned messages and messages that are more than 14 days old
    messagesToDelete = fetchedMessages.filter(
      (msg) => !msg.pinned && Date.now() - msg.createdTimestamp < 14 * 24 * 60 * 60 * 1000
    );
    // Bulk delete the filtered messages
    await channel.bulkDelete(messagesToDelete);
    await delay(2000); // 2 second delay
  } while (messagesToDelete.size !== 0); // Repeat until there are no more messages to delete

  // Delete individual messages older than 14 days
  await delay(2000); // 2 second delay
  const fetchedMessages = await channel.messages.fetch({ limit: 100 });
  const oldUnpinnedMessages = fetchedMessages.filter((msg) => !msg.pinned && Date.now() - msg.createdTimestamp >= 14 * 24 * 60 * 60 * 1000);
  for (const msg of oldUnpinnedMessages.values()) {
    await msg.delete();
    await delay(1000); // 1 second delay
  }
}

export function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
