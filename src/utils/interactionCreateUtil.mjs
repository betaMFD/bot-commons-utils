export async function handleCommandInteraction(interaction, bot) {
  // need an interaction
  if (!interaction) {
    console.error("Need 'interaction' to be passed in to use handleCommandInteraction().");
  }

  // need a bot
  if (!bot) {
    console.error("Need 'bot' to be passed in to use handleCommandInteraction().");
  }

  // If the interaction isn't a command, don't bother continuing
  if (!interaction.isCommand()) return;

  // Find the command on the bot's commands
  const command = interaction.client.commands.get(interaction.commandName);
  if (!command) return;

  // Try to perform the command
  try {
    await command.execute(interaction, bot);
  } catch (error) {
    console.error(error);
    await handleError(interaction, 'There was an error while executing this command!');
  }
}

export async function handleError(interaction, errorMessage) {
  const errorReply = { content: errorMessage, ephemeral: true };
  if (interaction.deferred) {
    await interaction.editReply(errorReply);
  } else if (interaction.replied) {
    await interaction.followUp(errorReply);
  } else {
    await interaction.reply(errorReply);
  }
}
