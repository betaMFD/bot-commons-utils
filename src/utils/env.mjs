import fs from 'fs';
import path from 'path';
import { parse } from 'dotenv';
import {loadCommonModules} from './importModulesInDir.mjs';

export async function ensureEnvironmentVariables(bot) {
  const envPath = path.join(bot.botRoot, '.env');
  const envDistPath = path.join(bot.botRoot, '.env.dist');

  const envExists = fs.existsSync(envPath);
  const envDistExists = fs.existsSync(envDistPath);

  if (!envExists || !envDistExists) {
    console.error("Missing .env or .env.dist files.");
    process.exit(1);
  }

  const envDistContent = fs.readFileSync(envDistPath, 'utf-8');
  const envContent = envExists ? fs.readFileSync(envPath, 'utf-8') : '';

  const envDistVars = parse(envDistContent);
  const envVars = parse(envContent);

  const { env: commonEnvVars } = await loadCommonModules(bot.botRoot);

  let missingVars = [];
  let addedVars = [];
  let extraVars = [];

  // Check for missing variables in .env that are defined in .env.dist or common modules
  for (const [key, defaultValue] of Object.entries(envDistVars)) {
    if (envVars[key] === undefined) {  // Changed from '!envVars[key]' to 'envVars[key] === undefined'
      if (defaultValue) {
        envVars[key] = defaultValue;  // Set default from .env.dist if not empty
        addedVars.push(key);
      } else {
        missingVars.push(key); // Mark as missing if no default value provided
      }
    }
  }

  // Check for variables required by common modules
  for (const commonVar of commonEnvVars) {
    if (envVars[commonVar] === undefined) {
      missingVars.push(commonVar);
    }
  }

  // Check for extra variables in .env that are not defined in .env.dist or common modules
  for (const key of Object.keys(envVars)) {
    if (!envDistVars.hasOwnProperty(key) && !commonEnvVars.includes(key)) {
      extraVars.push(key);
    }
  }

  // Update .env with added variables
  if (addedVars.length > 0) {
    const updatedEnvContent = Object.entries(envVars).map(([key, value]) => `${key}=${value}`).join('\n');
    fs.writeFileSync(envPath, updatedEnvContent); // Write back to .env file with added defaults
    addedVars.forEach(varName => console.log(`Added default value for ${varName} to .env`));
  }

  // Log any extra variables
  if (extraVars.length > 0) {
    console.log("Extra variables in .env that are not in .env.dist or common modules:", extraVars.join(', '));
  }

  // Exit if there are missing variables
  if (missingVars.length > 0) {
    console.error("Missing required environment variables:", missingVars.join(', '));
    process.exit(1);
  }
}
