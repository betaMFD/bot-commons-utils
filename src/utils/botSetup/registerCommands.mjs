import {importModulesInDir, loadCommonModules} from '../importModulesInDir.mjs';
import {Collection} from 'discord.js';
import path from 'node:path';

export async function registerCommands(bot, folder='src/commands') {
  const commonModules = await loadCommonModules(bot.botRoot);

  bot.client.commands = new Collection();

  // Import and register commands from the folder
  for await (const module of importModulesInDir(path.resolve(bot.botRoot, folder), { recursive: true })) {
    bot.client.commands.set(module.data.name, wrapCommandWithBot(module, bot));
  }

  // Import and register common commands
  for (const commandModule of commonModules.commands) {
    bot.client.commands.set(commandModule.data.name, wrapCommandWithBot(commandModule, bot));
  }
}

function wrapCommandWithBot(commandModule, bot) {
  // Wrap the execute function to include bot
  const wrappedModule = {
    ...commandModule,
    execute: async (interaction) => {
      await commandModule.execute(interaction, bot); // Pass bot to the execute function
    },
  };
  return wrappedModule;
}
