import {importModulesInDir, loadCommonModules} from '../importModulesInDir.mjs';
import {Collection} from 'discord.js';
import path from 'node:path';

export async function registerScripts(bot, folder='src/scripts') {
  const commonModules = await loadCommonModules(bot.botRoot);

  bot.client.scripts = new Collection();
  for await (const scriptModule of importModulesInDir(path.resolve(bot.botRoot, folder))) {
    const scriptNames = Object.keys(scriptModule);

    // Loop through each script in the file and add it to the collection
    for (const scriptName of scriptNames) {
      const script = scriptModule[scriptName];

      // Only add objects with regex and execute functions to the scripts collection
      if (script && script.regex && typeof script.execute === 'function') {
        bot.client.scripts.set(scriptName, {
          ...script,
          execute: (message) => script.execute(message, bot), // Wrap execute to include bot
        });
      }
    }
  }
  // Bot-Commons
  for (const scriptModule of commonModules.scripts) {
    const script = scriptModule.module;
    if (script && script.regex && typeof script.execute === 'function') {
      bot.client.scripts.set(scriptModule.name, {
        ...script,
        execute: (message) => script.execute(message, bot), // Wrap execute to include bot
      });
    }
  }
}
