import {initBrain} from '../brain.mjs';
import {ensureEnvironmentVariables} from '../env.mjs';
import { config } from 'dotenv';

export async function firstSteps(bot) {
  // Initialize brain
  await initBrain();
  // Ensure all environment variables are set correctly
  await ensureEnvironmentVariables(bot);
  // Set up environment variables
  config();
}
