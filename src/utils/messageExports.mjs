import { Message } from 'discord.js';

/**
 * Gathers messages from a channel based on optional start and end dates.
 *
 * @param {Channel} channel - The channel to gather messages from.
 * @param {Date} [startDate] - The start date to gather messages from.
 * @param {Date} [endDate] - The end date to gather messages up to.
 * @param {number} [limit=100] - The limit of messages to fetch per request (default is 100).
 * @returns {Promise<Message[]>} - A promise that resolves to an array of messages.
 */
export async function gatherMessages(channel, startDate, endDate, limit = 100) {
  if (!channel) {
    throw new Error('No channel given.');
  }

  let messages = [];
  let lastId;

  while (true) {
    const options = { limit };
    if (lastId) options.before = lastId;

    const fetchedMessages = await channel.messages.fetch(options);
    if (fetchedMessages.size === 0) break;

    const filteredMessages = fetchedMessages.filter(message => {
      if (startDate && message.createdAt < startDate) return false;
      if (endDate && message.createdAt > endDate) return false;
      return true;
    });

    messages = messages.concat(Array.from(filteredMessages.values()));

    if (startDate && fetchedMessages.some(message => message.createdAt < startDate)) break;
    lastId = fetchedMessages.last().id;
  }

  return messages.reverse(); // Reverse to maintain chronological order
}


/**
 * Gathers messages from multiple channels and sorts them in chronological order.
 *
 * @param {Array<Channel>} channels - An array of channels to gather messages from.
 * @param {Date} [startDate] - The start date to gather messages from.
 * @param {Date} [endDate] - The end date to gather messages up to.
 * @param {number} [limit=100] - The limit of messages to fetch per request (default is 100).
 * @returns {Promise<Message[]>} - A promise that resolves to an array of messages sorted in chronological order.
 */
export async function gatherMessagesFromMultipleChannels(channels, startDate, endDate, limit = 100) {
  let allMessages = [];

  for (const channel of channels) {
    const messages = await gatherMessages(channel, startDate, endDate, limit);
    allMessages = allMessages.concat(messages);
  }

  // Sort all messages in chronological order
  allMessages.sort((a, b) => a.createdTimestamp - b.createdTimestamp);

  return allMessages;
}


/**
 * Handles the embeds in a message and appends their details to the formatted message string.
 *
 * @param {Message} message - The message containing embeds.
 * @param {string} formattedMessage - The formatted message string to append embed details to.
 * @returns {string} - The formatted message string with embed details appended.
 */
export function handleEmbeds(message, formattedMessage) {
  if (message.embeds.length > 0) {
    message.embeds.forEach(embed => {
      if (embed.title) formattedMessage += `Embed Title: ${embed.title}\n`;
      if (embed.description) formattedMessage += `Embed Description: ${embed.description}\n`;
      if (embed.fields && embed.fields.length > 0) {
        formattedMessage += 'Embed Fields:\n';
        embed.fields.forEach(field => {
          formattedMessage += `  - ${field.name}: ${field.value}\n`;
        });
      }
    });
  }
  return formattedMessage;
}

/**
 * Formats a message into a string, optionally including the creation date.
 *
 * @param {Message} message - The message to format.
 * @param {boolean} [includeDates=true] - Whether to include the creation date in the formatted message.
 * @returns {string} - The formatted message string.
 */
export function formatMessage(message, includeDates = true) {
  let formattedMessage = includeDates
    ? `[${message.createdAt}] ${message.author.tag}: ${message.content}\n`
    : `${message.author.tag}: ${message.content}\n`;

  return handleEmbeds(message, formattedMessage);
}
